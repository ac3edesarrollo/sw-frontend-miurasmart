import { Deserializable } from "app/shared/models/deserializable"

export class Alerta implements Deserializable {

    id: number
    time_stamp: string
    bolso_id: number
    bolso_name: string
    user_id: number
    alert_name: string
    latitud: string
    longitud: string
    status: string
    deserialize(input: any): this {
        return Object.assign(this, input);
    }
}
import { Deserializable } from "app/shared/models/deserializable"

export class Usuario implements Deserializable {
    user_id: number;
    nombre : string;
    apellido : string;
    direccion : string;
    email: string;
    user_name : string;
    perfil : string;
    last_login : string;
    date_birth : string;
    foto : string;
    deserialize(input: any): this {
        return Object.assign(this, input);
    }
}
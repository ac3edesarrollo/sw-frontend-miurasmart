import { Deserializable } from "app/shared/models/deserializable"

export class Reporte implements Deserializable {

    id: number
    time_stamp: string
    bolso_id: number
    user_id: number
    latitud: number
    longitud: number
    deserialize(input: any): this {
        return Object.assign(this, input);
    }
}
import { Deserializable } from "app/shared/models/deserializable"

export class Bolso implements Deserializable {

    id: number
    tipo: string
    modelo: string
    namebyuser: string
    product_name: string
    imagen: string
    date_create: string
    deleted_on: string
    user_id: number
    register_date: string
    activo: boolean
    deserialize(input: any): this {
        return Object.assign(this, input);
    }
}
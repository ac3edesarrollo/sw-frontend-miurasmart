export interface User {
  id?: string;
  displayName?: string;
  role?: string;
  date_birth?: string;
  last_login?: string;
  firstname?: string;
  lastname?: string;
  email?: string;
  direccion?: string;
  foto?: string;
}
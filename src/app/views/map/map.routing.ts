import { Routes } from '@angular/router';
import { MapComponent } from './map.component';
//import { FormSelectComponent } from './map-selector/map-selector.component';

export const MapRoutes: Routes = [
  { 
    path: '', 
    component: MapComponent, 
    data: { title: 'Ubicaciones', breadcrumb: 'Ubicaciones'}, 
    // children: [
    //   {
    //     path: '', 
    //     component: FormSelectComponent, 
    //     //data: { title: 'Ubicaciones', breadcrumb: 'Ubicaciones'} 
    //   }
    // ]
  },
];
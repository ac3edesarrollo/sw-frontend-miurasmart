import { Component, OnInit } from '@angular/core';
import { MapService } from './map.service';
import { Bolso } from "../../shared/models/bolso";
import {Reporte } from "../../shared/models/reporte";
import { CloseScrollStrategy } from '@angular/cdk/overlay';
import { isNull } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})

export class MapComponent implements OnInit {
  selectedValue: string;
  public bolsosArray: Array<Bolso> = [];
  public reporteArray: Array<Reporte> = [];

  zoom = 12;
  mapCenter = {
    bag_id: 0,
    lat: -33.024788,
    lng: -71.551819,
    time_: "2020"
  }
  polylinePoints = [
    { lat: 24.847916, lng: 89.369764 },
    { lat: 23.806921, lng: 90.377078 },
    { lat: 24.919298, lng: 91.831699 }
  ];
  circleMapRadius = 50000;

  constructor(
    private mapService: MapService,
  ) { }

  ngOnInit() {
    this.getItemsbolsos()
  }

  circleMapRadiusChange(radius) {
    this.circleMapRadius = radius;
    // console.log(e)
  }

  getItemsbolsos() {
    this.mapService.getBolsos().subscribe(data => {
      this.bolsosArray = [...this.bolsosArray]
      this.bolsosArray = data;
    });
  }

  onBolsoChange() {
    this.mapService.getReporte(this.selectedValue).subscribe(data => {
      if (data.length === 0) {
        this.mapCenter = {
          bag_id: NaN,
          lat: -33.024788,
          lng: -71.551819,
          time_: "No existen registros para este Dispositivo"
      }
      }
      else {
        this.reporteArray = [...this.reporteArray]
        this.reporteArray = data;
        this.mapCenter = {
          bag_id: this.reporteArray[0].bolso_id,
          lat: this.reporteArray[0].latitud,
          lng: this.reporteArray[0].longitud,
          time_: this.reporteArray[0].time_stamp
        }
      }
      // console.log( this.reporteArray[0].id, this.reporteArray[0].latitud, this.reporteArray[0].longitud, this.reporteArray[0].time_stamp)
    });
  }

}

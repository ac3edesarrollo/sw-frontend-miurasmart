import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserDB } from '../../shared/inmemory-db/users';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';
import { Bolso } from "../../shared/models/bolso";
import { Reporte } from "../../shared/models/reporte";
import { FormGroup } from '@angular/forms';
import { environment } from "environments/environment";

@Injectable()
export class MapService {
  baseURL = environment.apiURL;
  items: any[];
  constructor(
    private http: HttpClient
  ) {
    let userDB = new UserDB();
    this.items = userDB.users;
  }

  //******* Implement your APIs ********
  getItems(): Observable<any> {
    return  of(this.items.slice())
  }

  getBolsos(): Observable<Bolso[]> {
    const URL = `${this.baseURL}/api/admin/bolsos/byuser`;
    const res = this.http.get<Bolso[]>(URL).pipe(data => {
      return data;
    });
    return res;
  }

  getReporte(id): Observable<Reporte[]> {
    const formData = new FormData();
    // console.log(id);
    formData.append('bolso_id',id.toString());
    const URL = `${this.baseURL}/api/admin/bolsos/reportes/bag`;
    const resp = this.http.put<Reporte[]>(URL,formData).pipe(data => {
        return data;
    });
    return resp;
  }

}

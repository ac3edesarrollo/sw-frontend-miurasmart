import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Alerta } from "../../shared/models/alertas";
import { environment } from "environments/environment";

@Injectable({
  providedIn: 'root'
})

export class InvoiceService {
  baseURL = environment.apiURL;

  constructor(private http: HttpClient) { }

  getInvoiceList() {
    return this.http.get('/api/invoices/');
  }

  getInvoiceById(id) {
    return this.http.get('/api/invoices/'+id);
  }
  
  saveInvoice(invoice) {
    if(invoice.id){
      return this.http.put('/api/invoices/'+invoice.id, invoice);
    } else {
      invoice.id = (Math.random() * 1000000000).toString();
      return this.http.post('/api/invoices/', invoice);
    }
  }

  deleteInvoice(id) {
    return this.http.delete('/api/invoices/'+id);
  }

  getAlertas(): Observable<Alerta[]> {
    const URL = `${this.baseURL}/api/admin/bolsos/alertas/userbag`;
    const res = this.http.get<Alerta[]>(URL).pipe(data => {
      return data;
    });
    return res;
  }

  deleteAlerta(id) {
    const formData = new FormData();
    // console.log(id);
    formData.append('id',id.toString());
    let URL = `${this.baseURL}/api/admin/bolsos/alertas/eliminar`
    // console.log(formData);
    const resp = this.http.post(URL,formData).subscribe(r=>{
      console.log(r); 
    })
    return resp;
  }
}

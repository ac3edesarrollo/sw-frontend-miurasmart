import { Component, OnInit, ViewChild, ChangeDetectorRef, ChangeDetectionStrategy } from "@angular/core";
import { MatTable } from "@angular/material/table";
import { InvoiceService } from "../invoice.service";
import { AppConfirmService } from "app/shared/services/app-confirm/app-confirm.service";
import { Invoice } from "app/shared/models/invoice.model";
import { Alerta } from "../../../shared/models/alertas";

@Component({
  selector: "app-invoice-list",
  templateUrl: "./invoice-list.component.html",
  styleUrls: ["./invoice-list.component.scss"]
})
export class InvoiceListComponent implements OnInit {
  @ViewChild(MatTable) itemTable: MatTable<any>;
  public alertasArray: Array<Alerta> = [];
  invoiceList: Invoice[];

  itemTableColumn: string[] = [
    "Bolso",
    "Hora",
    "Alerta",
    "Estatus",
    "Acciones"
  ];

  constructor(
    private invoiceService: InvoiceService,
    private confirmService: AppConfirmService,
    private cdr: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.getItemsalertas();
  }

  deleteAlertaById(id) {
    this.confirmService
      .confirm({ title: "Confirmar", message: "¿Está seguro que quiere borrar el registro?" })
      .subscribe(res => {
        if (res) {
          this.invoiceService.deleteAlerta(id)
          this.getItemsalertas()
        } else return;
      });
  }

  getItemsalertas() {
  this.invoiceService.getAlertas().subscribe(data => {
    this.alertasArray = [...this.alertasArray]
    this.alertasArray = data;
  });
}
}

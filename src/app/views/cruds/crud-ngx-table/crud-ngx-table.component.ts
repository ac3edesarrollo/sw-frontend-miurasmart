import { Component, OnInit, OnDestroy } from '@angular/core';
import { CrudService } from '../crud.service';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppConfirmService } from '../../../shared/services/app-confirm/app-confirm.service';
import { AppLoaderService } from '../../../shared/services/app-loader/app-loader.service';
import { NgxTablePopupComponent } from './ngx-table-popup/ngx-table-popup.component';
import { Subscription } from 'rxjs';
import { egretAnimations } from "../../../shared/animations/egret-animations";
import { Bolso } from "../../../shared/models/bolso";

@Component({
  selector: 'app-crud-ngx-table',
  templateUrl: './crud-ngx-table.component.html',
  animations: egretAnimations
})
export class CrudNgxTableComponent implements OnInit {
  public items: any[];
  public bolsosArray: Array<Bolso> = [];
;
  constructor(
    private dialog: MatDialog,
    private snack: MatSnackBar,
    private crudService: CrudService,
    private confirmService: AppConfirmService,
    private loader: AppLoaderService
  ) { }

  ngOnInit() {
    this.getItemsbolsos()
  }

  getItemsbolsos() {
    this.crudService.getBolsos().subscribe(data => {
      this.bolsosArray = [...this.bolsosArray]
      this.bolsosArray = data;
    });
  }

  openPopUp(data: any = {}, isNew?) {
    let title = isNew ? 'Agregar Nuevo Bolso' : 'Editar Bolso';
    let dialogRef: MatDialogRef<any> = this.dialog.open(NgxTablePopupComponent, {
      width: '720px',
      disableClose: true,
      data: { title: title, payload: data }
    })
    dialogRef.afterClosed()
      .subscribe(res => {
        if(!res) {
          // If user press cancel
          return;
        }
        this.loader.open();
        if (isNew) {
          this.crudService.addBolso(res)
          this.loader.close();
          this.getItemsbolsos()
          this.snack.open('¡Bolso Agregado!', 'Aceptar', { duration: 4000 })
        } else {
          this.crudService.editBolso(data.id, res)
          this.loader.close();
          this.getItemsbolsos();
          this.snack.open('¡Bolso Editado!', 'Aceptar', { duration: 4000 })
            
        }
      })
  }
  deleteItem(row) {
    this.confirmService.confirm({ title: "Confirmar", message: `¿Seguro que desea eliminar el Bolso ${row.name}?`})
      .subscribe(res => {
        if (res) {
          this.loader.open();
            this.crudService.deleteBolso(row)
            this.loader.close();
            this.getItemsbolsos()
            this.snack.open('¡Bolso Eliminado!', 'Aceptar', { duration: 4000 })
        }
      })
  }
}
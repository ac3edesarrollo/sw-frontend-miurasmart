import {Deserializable} from './deserializable'
import { map } from 'rxjs/operators'

export class Totem implements Deserializable{
    id:number
    name: string
    location: string
    sub_unidades: Array<SubUnidades>

    deserialize(input: any): this{
        Object.assign(this,input);
        this.sub_unidades = input.sub_unidades.map(subUnidad => new SubUnidades().deserialize(subUnidad))
        return this;
    }
}

export class SubUnidades{
    id: number
    name: string
    user_id: number
    quest: string

    deserialize(input: any): this{
        return Object.assign(this,input);
    }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserDB } from '../../shared/inmemory-db/users';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';
import { Usuario } from "../../shared/models/usuario";
import { FormGroup } from '@angular/forms';
import { environment } from "environments/environment";

@Injectable()
export class ProfileService {
  baseURL = environment.apiURL;
  items: any[];
  constructor(
    private http: HttpClient
  ) {
    let userDB = new UserDB();
    this.items = userDB.users;
  }

  //******* Implement your APIs ********
  getItems(): Observable<any> {
    return  of(this.items.slice())
  }

  getUsuarios(): Observable<Usuario[]> {
    const URL = `${this.baseURL}/api/admin/users/cargar`;
    const res = this.http.get<Usuario[]>(URL).pipe(data => {
      return data;
    });
    return res;
  }

  getUsuario(): Observable<Usuario[]> {
    const URL = `${this.baseURL}/api/admin/login/usuario/token`;
    const res = this.http.get<Usuario[]>(URL).pipe(data => {
      return data;
    });
    return res;
  }

  addUsuario(item) {
    const formData = new FormData();
    console.log(item);
    formData.append('nombre',item.nombre);
    formData.append('apellido',item.apellido);
    formData.append('direccion',item.direccion);
    formData.append('email',item.email);
    formData.append('user_name',item.user_name);
    formData.append('password',item.password);
    formData.append('perfil',item.perfil);
    formData.append('date_birth',item.date_birth);
    let URL = `${this.baseURL}/api/admin/users/agregar`
    const resp = this.http.post(URL,formData).subscribe(r=>{
      console.log(r); 
    })
    return resp;
  }
   
  editUsuario(id,item) {
    const formData = new FormData();
    formData.append('user_id',item.id);
    formData.append('nombre',item.nombre);
    formData.append('apellido',item.apellido);
    formData.append('direccion',item.direccion);
    formData.append('email',item.email);
    formData.append('user_name',item.user_name);
    // formData.append('password',item.password);
    formData.append('perfil',item.perfil);
    formData.append('date_birth',item.date_birth);
    let URL = `${this.baseURL}/api/admin/users/editar`
    const resp = this.http.put(URL,formData).subscribe(r=>{
      console.log(r); 
    })
    return resp;
  }

  deleteUser(id) {
    const formData = new FormData();
    console.log(id);
    formData.append('user_id',id.id.toString());
    let URL = `${this.baseURL}/api/admin/users/eliminar`
    const resp = this.http.put(URL,formData).subscribe(r=>{
      console.log(r); 
    })
    return resp;
  }

  getUsuario_byid(id) {
    const formData = new FormData();
    console.log(id);
    formData.append('user_id',id.id.toString());
    let URL = `${this.baseURL}/api/admin/login/usuario/token`
    const resp = this.http.put(URL,formData).subscribe(r=>{
      console.log(r); 
    })
    return resp;
  }

}
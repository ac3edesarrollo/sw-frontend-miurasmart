import { Component, OnInit } from '@angular/core';
import { FileUploader } from 'ng2-file-upload';
import { User } from '../../../shared/models/user.model';
import { Usuario } from '../../../shared/models/usuario';
import { ActivatedRoute } from "@angular/router";
import { JwtAuthService } from "app/shared/services/auth/jwt-auth.service";
import { AppLoaderService } from '../../../shared/services/app-loader/app-loader.service';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { ProfileSettingPopupComponent } from './profile-settings-popup/profile-settings-popup.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';
import { number } from 'ngx-custom-validators/src/app/number/validator';
import { ProfileService } from '../profile.service';
import { environment } from "environments/environment";

@Component({
  selector: 'app-profile-settings',
  templateUrl: './profile-settings.component.html',
  styleUrls: ['./profile-settings.component.css']
})

export class ProfileSettingsComponent implements OnInit {
  baseURL = environment.apiURL;
  public uploader: FileUploader = new FileUploader({ url: `${this.baseURL}/api/filestorage/save` });
  public hasBaseDropZoneOver: boolean = false;
  public userArray: Array<Usuario> = [];
  //activeView: string = "overview";
  public _id_: string;
  user: Observable<User>;
  token;

  constructor(
    private router: ActivatedRoute, 
    public jwtAuth: JwtAuthService,
    private loader: AppLoaderService,
    private dialog: MatDialog,
    private snack: MatSnackBar,
    private profiles: ProfileService
    ) { }

  ngOnInit() {
    //this.activeView = this.router.snapshot.params["view"];
    this.inicio()
    this.actualUser()
  }

  inicio() {
    this.user = this.jwtAuth.user$;
    this.token=this.jwtAuth.getJwtToken();
  }

  subir() {
    this.uploader.authToken=this.token;
    this.uploader.uploadAll();
  }

  actualUser() {
    this.profiles.getUsuario().subscribe(data => {
      this.userArray = [...this.userArray]
      this.userArray = data;
      // console.log(this.userArray)
    });
  }

  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  openPopUp(data: any = {}, isNew?) {
    // console.log(data)
    let title = isNew ? 'Agregar Nuevo Usuario' : 'Editar Usuario';
    let dialogRef: MatDialogRef<any> = this.dialog.open(ProfileSettingPopupComponent, {
      width: '720px',
      disableClose: true,
      data: { title: title, payload: data }
    })
    dialogRef.afterClosed()
      .subscribe(res => {
        if(!res) {
          // If user press cancel
          return;
        }
        this.loader.open();
        if (isNew) {
          this.profiles.addUsuario(res)
          this.loader.close();
          this.snack.open('¡Usuario Agregado!', 'Aceptar', { duration: 4000 })
        } else {
          this.profiles.editUsuario(data, res)
          this.loader.close();
          this.snack.open('¡Usuario Editado!', 'Aceptar', { duration: 4000 })            
        }
        this.inicio();
      })
  }

}

import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-profile-settings-popup',
  templateUrl: './profile-settings-popup.component.html'
})
export class ProfileSettingPopupComponent implements OnInit {
  public itemForm: FormGroup;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<ProfileSettingPopupComponent>,
    private fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.buildItemForm(this.data.payload)
  }
  buildItemForm(item) {
    this.itemForm = this.fb.group({
      id: [item.id || ''],
      nombre_usuario: [item.user_name || ''], 
      nombre: [item.nombre || '', Validators.required],
      apellido: [item.apellido || ''],
      direccion: [item.direccion || ''],
      email: [item.email || ''],
      user_name: [item.user_name || ''],
      perfil: [item.perfil || ''],
      date_birth: [item.date_birth || ''],
    })
  }

  submit() {
    this.dialogRef.close(this.itemForm.value)
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserDB } from '../../shared/inmemory-db/users';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';
import { Bolso } from "../../shared/models/bolso";
import { FormGroup } from '@angular/forms';
import { environment } from "environments/environment";

@Injectable()
export class ProductService {
  baseURL = environment.apiURL;
  items: any[];
  constructor(
    private http: HttpClient
  ) {
    let userDB = new UserDB();
    this.items = userDB.users;
  }

  //******* Implement your APIs ********
  getItems(): Observable<any> {
    return  of(this.items.slice())
  }

  getBolsos(): Observable<Bolso[]> {
    const URL = `${this.baseURL}/api/admin/bolsos/byuser`;
    const res = this.http.get<Bolso[]>(URL).pipe(data => {
      return data;
    });
    return res;
  }

  addItem(item): Observable<any> {
    item._id = Math.round(Math.random() * 10000000000).toString();
    this.items.unshift(item);
    return of(this.items.slice()).pipe(delay(1000));
  }

  updateItem(id, item) {
    this.items = this.items.map(i => {
      if(i._id === id) {
        return Object.assign({}, i, item);
      }
      return i;
    })
    return of(this.items.slice()).pipe(delay(1000));
  }

  removeItem(row) {
    let i = this.items.indexOf(row);
    this.items.splice(i, 1);
    return of(this.items.slice()).pipe(delay(1000));
  }

  addBolso(item) {
    const formData = new FormData();
    // console.log(item);
    if (item.isActive == ''){
      item.isActive = false
    }
    console.log(item);
    formData.append('tipo',item.tipo);
    formData.append('product_name',item.product_name);
    formData.append('activo',item.isActive.toString());
    // formData.append('imagen',item.value.imagen);
    // formData.append('data_create',item.value.data_create);
    // formData.append('deleted_on',item.value.deleted_on);
    let URL = `${this.baseURL}/api/admin/bolsos/agregar`
    // console.log(formData);
    const resp = this.http.post(URL,formData).subscribe(r=>{
      console.log(r); 
    })
    return resp;
  }
   
  editBolso(item,id) {
    const formData = new FormData();
    // console.log(id);
    formData.append('bolso_id',id.id.toString());
    formData.append('tipo',id.tipo);
    formData.append('modelo',id.modelo);
    formData.append('namebyuser',id.namebyuser);
    formData.append('user_id',id.user_id);
    formData.append('product_name',id.product_name);
    formData.append('activo',id.isActive.toString());
    // formData.append('imagen',item.value.imagen);
    // formData.append('data_create',item.value.data_create);
    // formData.append('deleted_on',item.value.deleted_on);
    let URL = `${this.baseURL}/api/admin/bolsos/editarbolso`
    // console.log(formData);
    const resp = this.http.put(URL,formData).subscribe(r=>{
      console.log(r); 
    })
    return resp;
  }

  deleteBolso(id) {
    const formData = new FormData();
    // console.log(id);
    formData.append('bolso_id',id.id.toString());
    let URL = `${this.baseURL}/api/admin/bolsos/eliminarbolso`
    // console.log(formData);
    const resp = this.http.put(URL,formData).subscribe(r=>{
      console.log(r); 
    })
    return resp;
  }
}
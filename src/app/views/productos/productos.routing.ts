import { Routes } from '@angular/router';
import { AppProductsComponent } from './productos-views/productos-views.component';

export const ProductsRoutes: Routes = [
  {
    path: '',
    children: [{
      path: 'lista',
      component: AppProductsComponent,
      data: { title: 'Productos', breadcrumb: 'Productos' }
    },]
  }
];
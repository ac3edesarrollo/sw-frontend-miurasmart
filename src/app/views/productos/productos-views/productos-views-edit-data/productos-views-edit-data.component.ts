import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'productos-views-edit-data',
  templateUrl: './productos-views-edit-data.component.html'
})
export class ProductsSettingDataComponent implements OnInit {
  public itemForm: FormGroup;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<ProductsSettingDataComponent>,
    private fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.buildItemForm(this.data.payload)
  }
  buildItemForm(item) {
    if (item.activo == 'true'){
      item.isActive = true
    } else {
      item.isActive = false
    }
    this.itemForm = this.fb.group({
      product_name: [item.product_name || '', Validators.required],
      tipo: [item.tipo || ''],
      modelo: [item.modelo || ''],
      id: [item.id || ''],
      imagen: [item.imagen || ''],
      namebyuser: [item.namebyuser || ''],
      date_create: [item.date_create || ''],
      deleted_on: [item.deleted_on || ''],
      user_id: [item.user_id || ''],
      register_date: [item.register_date || ''],
      isActive: [item.isActive || '']
    })
  }

  submit() {
    this.dialogRef.close(this.itemForm.value)
  }
}

import { Component, OnInit } from '@angular/core';
import { ProductService } from '../productos.service';
import { Bolso } from 'app/shared/models/bolso';
import { ProductsSettingDataComponent } from './productos-views-edit-data/productos-views-edit-data.component';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { AppLoaderService } from 'app/shared/services/app-loader/app-loader.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppConfirmService } from 'app/shared/services/app-confirm/app-confirm.service';

@Component({
  selector: 'productos-views',
  templateUrl: './productos-views.component.html',
  styleUrls: ['./productos-views.component.css']
})
export class AppProductsComponent implements OnInit {
  public productosArray: Array<Bolso> = [];
  
  constructor(
    private productService: ProductService,
    private confirmService: AppConfirmService,
    private dialog: MatDialog,
    private loader: AppLoaderService,
    private snack: MatSnackBar,
  ) { }

  ngOnInit() {
    this.getItemsBolsos()
  }

  getItemsBolsos() {
    this.productService.getBolsos().subscribe(data => {
      this.productosArray = [...this.productosArray]
      this.productosArray = data;
      // console.log(this.productosArray)
    });
  }

  openPopUp(data: any = {}, isNew?) {
    // console.log(data)
    let title = isNew ? 'Agregar Nuevo Producto' : 'Editar Producto';
    let dialogRef: MatDialogRef<any> = this.dialog.open(ProductsSettingDataComponent, {
      width: '720px',
      disableClose: true,
      data: { title: title, payload: data }
    })
    dialogRef.afterClosed()
      .subscribe(res => {
        if(!res) {
          // If user press cancel
          return;
        }
        this.loader.open();
        if (isNew) {
          this.productService.addBolso(res)
          this.loader.close();
          this.getItemsBolsos()
          this.snack.open('¡Usuario Agregado!', 'Aceptar', { duration: 4000 })
        } else {
          this.productService.editBolso(data, res)
          this.loader.close();
          this.getItemsBolsos()
          this.snack.open('¡Usuario Editado!', 'Aceptar', { duration: 4000 })            
        }
      })
  }

  deleteItemProducto(row) {
    this.confirmService.confirm({ title: "Confirmar", message: `¿Seguro que desea eliminar el Producto ${row.modelo}?`})
      .subscribe(res => {
        if (res) {
          this.loader.open();
            this.productService.deleteBolso(row)
            this.loader.close();
            this.getItemsBolsos()
            this.snack.open('¡Producto Eliminado!', 'Aceptar', { duration: 4000 })
        }
      })
  }
}
